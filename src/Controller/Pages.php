<?php

namespace Drupal\bitaps\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Pages {

	/**
   * Bitaps service.
   *
   * @var \Drupal\bitaps\Bitaps
   */
  protected $bitaps;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->bitaps = \Drupal::service('Bitaps');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
  }

	public function title(array $_title_arguments = array(), $_title = ''){
    return $this->bitaps->t($_title);
  }

  public function pages($page_type) {
  	$element = [];
  	switch($page_type) {
  		case'pay':
        $is_404 = TRUE;
        $payment = $this->bitaps->load([
          'id' => $this->query['pay_id'] ?? '-'
        ]);
        if(!empty($payment) && $payment->status == 'new') {
          $is_404 = FALSE;
          $element['form'] = \Drupal::formBuilder()->getForm('\Drupal\bitaps\Form\PaymentForm', $payment);
        }
        if($is_404){
          throw new NotFoundHttpException();
        }
        break;
      case'status':
        if (!$_POST) {
          $_POST = @json_decode(file_get_contents('php://input'), true);
        }
        $config = \Drupal::config('bitaps.settings')->get('config');
        $event = isset($_POST['event']) ? trim($_POST['event']) : FALSE;
        if(isset($_GET['hash']) && isset($_GET['oid']) && isset($_GET['amount']) && !empty($event)) {
          $payment = $this->bitaps->load([
            'id' => $_GET['oid']
          ]);
          if(!empty($payment)) {
            $hash = $this->bitaps->getHash($payment->id, $_GET['amount'], $config);
            if($hash == $_GET['hash']) {
              // ---
              $prePay = @unserialize($payment->data);
              $data['pre_pay'] = !empty($prePay['pre_pay']) ? $prePay['pre_pay'] : $prePay;
              // ---
              $payment->paytime = time();
              $payment->data = serialize($data);
              $payment->status = $event;
              // ---
              $this->bitaps->update($payment);
              // ---
              $event = md5($event);
              switch ($event) {
                case md5('confirmed'):
                  // ---
                  if(!empty($payment->nid) && !empty($this->basket)) {
                    if(method_exists($this->basket, 'paymentFinish')) {
                      $this->basket->paymentFinish($payment->nid);
                    }
                  }
                  // Alter
                  \Drupal::moduleHandler()->alter('bitaps_api', $payment, $_POST);
                  // ---
                break;
              }
              // Trigger change payment status by order
              if(!empty($payment->nid) && !empty($this->basket)){
                $order = $this->basket->Orders(NULL, $payment->nid)->load();
                if(!empty($order) && $this->moduleHandler->moduleExists('basket_noty')){
                  \Drupal::service('BasketNoty')->trigger('change_bitaps_status', [
                    'order' => $order
                  ]);
                }
              }
            }
          }
        }
        print $_POST['invoice'] ?? 'ERROR';
        exit;
        break;
  	}
  	return $element;
  }
}
