<?php

namespace Drupal\bitaps;

use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class Bitaps.
 */
class Bitaps {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Constructs a new Bitaps object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function t($string, $args = []){
    return new TranslatableMarkup($string, $args, ['context' => 'bitaps']);
  }

  /**
   * {@inheritdoc}
   */
  public function load($params) {
    $query = $this->database->select('payments_bitaps', 'l');
    $query->fields('l');
    if(!empty($params['id'])) {
      $query->condition('l.id', $params['id']);
    }
    if(!empty($params['nid'])) {
      $query->condition('l.nid', $params['nid']);
    }
    if(!empty($params['sid'])) {
      $query->condition('l.sid', $params['sid']);
    }
    if(empty($params['data'])) {
      $params['data'] = [];
    }
    $payment = $query->execute()->fetchObject();
    // ---
    if(!empty($params['create_new'])) {
      $payment = FALSE;
    }
    if(empty($payment) && !empty($params['amount'])){
      $payment = (object)[
        'nid' => !empty($params['nid']) ? $params['nid'] : NULL,
        'sid' => !empty($params['sid']) ? $params['sid'] : NULL,
        'uid' => \Drupal::currentUser()->id(),
        'created' => time(),
        'paytime' => NULL,
        'amount' => $params['amount'],
        'currency' => \Drupal::config('bitaps.settings')->get('config.currency'),
        'status' => 'new',
        'data' => serialize($params['data'])
      ];
      $payment->id = $this->database->insert('payments_bitaps')
        ->fields((array)$payment)
        ->execute();
    }
    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  public function update($payment) {
    if(!empty($payment->id)){
      $updateFields = (array)$payment;
      unset($updateFields['id']);
      $this->database->update('payments_bitaps')
        ->fields($updateFields)
        ->condition('id', $payment->id)
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getHash($pid, $amount, $config) {
    return hash('sha256', implode('~', [$pid, $config['secret_key'], $amount, $config['secret_key'], $pid]));
  }
}
