<?php

namespace Drupal\bitaps\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * Bitaps service.
   *
   * @var \Drupal\bitaps\Bitaps
   */
  protected $bitaps;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->bitaps = \Drupal::service('Bitaps');
    $this->ajax = [
      'wrapper' => 'bitaps_settings_form_ajax_wrap',
      'callback' => '::ajaxSubmit'
    ];
    $this->config = \Drupal::config('bitaps.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitaps_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form += [
      '#prefix' => '<div id="'.$this->ajax['wrapper'].'">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages'
      ]
    ];
    $form['config'] = [
      '#tree' => TRUE,
      'secret_key' => [
        '#type' => 'textfield',
        '#title' => 'Secret KEY',
        '#required' => TRUE,
        '#default_value' => $this->config->get('config.secret_key'),
      ],
      'forwarding_address' => [
        '#type' => 'textfield',
        '#title' => $this->bitaps->t('Forwarding address'),
        '#required' => TRUE,
        '#default_value' => $this->config->get('config.forwarding_address'),
      ],
      'confirmations' => [
        '#type' => 'number',
        '#title' => $this->bitaps->t('The number of confirmations required for the payment to be credited'),
        '#required' => TRUE,
        '#default_value' => $this->config->get('config.confirmations'),
      ],
      'currency' => [
        '#type' => 'select',
        '#options' => [
          'BTC' => 'BTC'
        ],
        '#title' => $this->bitaps->t('Currency'),
        '#default_value' => $this->config->get('config.currency'),
        '#required' => TRUE
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#name' => 'save',
        '#value' => t('Save configuration'),
        '#attributes' => [
          'class' => ['button--primary']
        ],
        '#ajax' => $this->ajax
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('bitaps.settings');
      $config->set('config', $form_state->getValue('config'));
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
