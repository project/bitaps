<?php

namespace Drupal\bitaps\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Class SettingsForm.
 */
class PaymentForm extends FormBase {

  const API_URL = 'https://api.bitaps.com/btc/v1/';

  /**
   * Bitaps service.
   *
   * @var \Drupal\bitaps\Bitaps
   */
  protected $bitaps;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->bitaps = \Drupal::service('Bitaps');
    $this->config = \Drupal::config('bitaps.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitaps_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = NULL) {
    // ---
    if(!empty($form['timer']))    unset($form['timer']);
    if(!empty($form['actions']))  unset($form['actions']);
    // ---
    $form['#id'] = 'payment_form';
    $form['info'] = [
      '#theme' => 'bitaps_pay',
      '#info' => [
        'label' => $this->bitaps->t('Sum'),
        'payment' => $payment
      ],
      '#attached' => [
        'library' => [
          'bitaps/css'
        ]
      ]
    ];
    // Payment alter
    $this->basketPaymentFormAlter($form, $form_state, $payment);
    // ---
    $form['info']['#info']['currency'] = $form['#params']['currency'];
    $form['info']['#info']['sum'] = $form['#params']['amount'];
    $form['info']['#info']['pay'] = $this->bitaps->t('Payment should be sent to this address @address@', [
      '@address@' => Markup::create('<strong>'. ($form['#params']['address'] ?? '- - -') .'</strong>')
    ]);
    // ---
    $form['mess'] = [
      '#type' => 'status_messages'
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->config->get('config');
    $form['#params'] = [
      'amount' => $payment->amount,
      'currency' => $payment->currency,
    ];
    // Alter
    \Drupal::moduleHandler()->alter('bitaps_payment_params', $form['#params'], $payment, $config);
    // ---
    $params = [
      'forwarding_address' => $config['forwarding_address'],
      'callback_link' => Url::fromRoute('bitaps.pages', [
        'page_type' => 'status'
      ], [
        'absolute' => TRUE,
        'query' => [
          'oid' => $payment->id,
          'amount' => $form['#params']['amount'],
          'hash' => $this->bitaps->getHash($payment->id, $form['#params']['amount'], $config)
        ]
      ])->toString(),
      'confirmations' => $config['confirmations'],
    ];
    // ---
    $form['#params']['address'] = $this->getAddress($params, $payment);
  }

  /**
   * @param array
   * @param object
   * @return string
   */

  private function getAddress($params, $payment) {
    $md5Params = md5(serialize($params));
    $pData = !empty($payment->data) ? unserialize($payment->data) : [];
    if(!empty($pData['md5Params'][$md5Params])) {
      return $pData['md5Params'][$md5Params];
    }
    try {
      $response = \Drupal::httpClient()->post($this::API_URL.'create/payment/address', [
        'body'   => json_encode($params)
      ]);
      $resData = $response->getBody()->getContents();
      if(!empty($resData)) {
        $resData = @json_decode($resData, TRUE);
        if(!empty($resData['error'])) {
          \Drupal::messenger()->addMessage($resData['error'], 'error');
        }
        elseif(!empty($resData['address'])) {
          $pData['md5Params'][$md5Params] = $resData['address'];
          $payment->data = serialize($pData);
          $this->bitaps->update($payment);
        }
      }
    } catch ( \Exception $e) {}

    return $pData['md5Params'][$md5Params] ?? NULL;
  }

}