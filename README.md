###### Change of parameters that go to the payment
```
<?php
function HOOK_bitaps_payment_params_alter(&$params, $payment, &$config){
	
}
```

###### Call upon payment, when the payment is linked to the site
```
<?php
function HOOK_bitaps_api_alter($payment, $fields){

}
```